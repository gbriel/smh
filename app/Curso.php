<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
	public function setTituloCursoAttribute($value)
	{
		$this->attributes['titulo_curso'] = $value;

		$value = preg_replace('/[áàãâä]/ui', 'a', $value);
		$value = preg_replace('/[éèêë]/ui', 'e', $value);
		$value = preg_replace('/[íìîï]/ui', 'i', $value);
		$value = preg_replace('/[óòõôö]/ui', 'o', $value);
		$value = preg_replace('/[úùûü]/ui', 'u', $value);
		$value = preg_replace('/[ç]/ui', 'c', $value);
		$value = strtolower($value);
		$value = preg_replace('/[^a-z0-9]/i', '-', $value);
		$value = preg_replace('/_+/', '-', $value);

		$this->attributes['link_curso'] = $value;
	}

	public function modulos()
	{
		return $this->hasMany('App\Modulo', 'curso_id', 'id');
	}
}
