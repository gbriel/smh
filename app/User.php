<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public static function boot()
    {
        parent::boot();

        self::saved(function($model){

            $videos = \App\Video::all();

            foreach($videos as $video){

                \App\Progresso::create([
                    'user_id' => $model->id,
                    'video_id' => $video->id
                ]);
            }
        });
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
