<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Progresso;

class ProgressoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function playing(Request $request)
	{
		Progresso::where('user_id', $request->user_id)->where('video_id', $request->video_id)->update(['progresso' => $request->progresso]);
	}
}
