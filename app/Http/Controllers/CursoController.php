<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;

class CursoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function cursos(Request $request)
	{
		return view('home')->with('cursos', Curso::all());
	}

	public function modulos(Request $request)
	{
		return view('modulos')->with('curso', Curso::where('link_curso', $request->curso)->get()->first());
	}
}
