<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function video(Request $request)
	{
		return response()->json([
			'video' => Video::find($request->id), 
			'progresso' => \App\Progresso::where('video_id', $request->id)->where('user_id', $request->user_id)->get()->first()->progresso
		]);
	}
}
