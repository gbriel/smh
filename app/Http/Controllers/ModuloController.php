<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modulo;

class ModuloController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function videos(Request $request)
	{
		return view('videos')->with('modulo', Modulo::where('link_modulo', $request->modulo)->get()->first());
	}
}
