<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
	
	public function setTituloModuloAttribute($value)
	{
		$this->attributes['titulo_modulo'] = $value;

		$value = preg_replace('/[áàãâä]/ui', 'a', $value);
		$value = preg_replace('/[éèêë]/ui', 'e', $value);
		$value = preg_replace('/[íìîï]/ui', 'i', $value);
		$value = preg_replace('/[óòõôö]/ui', 'o', $value);
		$value = preg_replace('/[úùûü]/ui', 'u', $value);
		$value = preg_replace('/[ç]/ui', 'c', $value);
		$value = strtolower($value);
		$value = preg_replace('/[^a-z0-9]/i', '-', $value);
		$value = preg_replace('/_+/', '-', $value);

		$this->attributes['link_modulo'] = $value;
	}

	public function curso()
	{
		return $this->belongsTo('App\Curso');
	}

	public function videos()
	{
		return $this->hasMany('App\Video', 'modulo_id', 'id');
	}

}
