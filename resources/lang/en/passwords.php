<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve conter pelo menos 6 caracteres e corresponder a confirmação.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Nós lhe enviamos um e-mail com o link de recuperação de senha!',
    'token' => 'Token invalido.',
    'user' => "Não foi possível encontrar um usuário cadastrado com este endereço.",

];
