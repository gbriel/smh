@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><a href="/cursos">Cursos</a>&nbsp&nbsp>&nbsp&nbsp{{ $curso->titulo_curso }}</div>

				<div class="card-body">
					@foreach($curso->modulos as $modulo)
					<div class="box col-md-4 col-lg-3">
						<a href="/curso/{{ $curso->link_curso }}/{{ $modulo->link_modulo }}/">
							<div modulo_id="{{ $modulo->id }}" class="video-box col-md-12 col-lg-12">
								<img src='{{ asset("storage/thumb/" . "modulo1.png") }}'>
								<span>{{ $modulo->titulo_modulo }}</span><br>
							</div>
						</a>
					</div>
					@endforeach
				</div>
				
			</div>
		</div>
	</div>
</div>
@endsection