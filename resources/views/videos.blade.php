@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><a href="/cursos">Cursos</a>&nbsp&nbsp>&nbsp
                    <a href="/curso/{{ $modulo->curso->link_curso }}">{{ $modulo->curso->titulo_curso }}</a>&nbsp&nbsp>&nbsp&nbsp{{ $modulo->titulo_modulo }}
                </div>

                <div class="card-body">
                    @foreach($modulo->videos as $video)
                    <div class="box col-md-4 col-lg-3">
                        <div video_id="{{ $video->id }}" class="video-box col-md-12 col-lg-12">
                            <img src='{{ asset("storage/thumb/" . $video->thumb_video) }}'>
                            <span>{{ $video->titulo_video }}</span><br>
                            <span class="duracao">Duração: {{ $video->duracao_video }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <video id="video" cod_video="" controls onplay="playing()" onpause="clearTimeout(clearPause)">
                    <source src='' />
                </video>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">

    var vid = HTMLMediaElement;

    $(document).on('click','.video-box',function(e) {

        var ele = $(this);

        $.ajax({
            url: "/video",
            type: "POST",
            dataType: "json",
            data: {
                id: $(this).attr('video_id'), 
                user_id: {{\Auth::id()}}
            },
            async: true,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                $('.modal-title').text(ele.children()[1].innerText);
                $('.modal-body > video').children().attr('src', '{{ asset("storage/videos/") }}/' + data.video.link_video);
                $('#video').attr('cod_video', data.video.id);

                $("#videoModal").modal('show');
                vid = document.getElementById("video");
                vid.load();
                vid.currentTime = data.progresso;

                $(document).on('hidden.bs.modal','#videoModal', function () {
                    vid.pause();
                });
            },
            error: function (x, t, m) {
                console.log(x.responseText);
            }
        });
    });

    var clearPause;

    function playing(){
        clearPause = setInterval(function(){
            $.ajax({
                url: "/progresso",
                type: "POST",
                dataType: "json",
                data: {
                    user_id: {{\Auth::id()}}, 
                    video_id: $('#video').attr('cod_video'), 
                    progresso: Math.round(vid.currentTime)
                },
                async: true,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }, 1000);
    }

</script>
@stop