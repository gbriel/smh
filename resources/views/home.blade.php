@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Cursos</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @foreach($cursos as $curso)
                    <div class="box col-md-4 col-lg-3">
                        <a href="/curso/{{ $curso->link_curso }}/">
                            <div video_id="{{ $curso->id }}" class="video-box col-md-12 col-lg-12">
                                <img src='{{ asset("storage/thumb/" . "modulo1.png") }}'>
                                <span>{{ $curso->titulo_curso }}</span><br>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection