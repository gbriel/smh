<?php

Route::view('/', 'welcome');

Auth::routes();

Route::get('/cursos', 'CursoController@cursos')->name('home');

Route::post('/video', 'VideoController@video');
Route::get('/curso/{curso}', 'CursoController@modulos');
Route::get('/curso/{curso}/{modulo}', 'ModuloController@videos');

Route::post('/progresso', 'ProgressoController@playing');