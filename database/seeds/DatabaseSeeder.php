<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(CursosTableSeeder::class);
    	DB::table('modulos')->insert([
    		[
    			'titulo_modulo' => 'Modulo I',
                'link_modulo' => 'modulo-i',
    			'curso_id' => 1,
    		],
    		[
    			'titulo_modulo' => 'Modulo II',
                'link_modulo' => 'modulo-ii',
    			'curso_id' => 1,
    		]
    	]);
    	DB::table('videos')->insert([
            [
                'modulo_id' => 1,
                'titulo_video' => 'Introdução',
                'duracao_video' => '00:07:56',
                'link_video' => 'M1A01.mp4',
                'thumb_video' => 'modulo2.png',
            ],
            [
                'modulo_id' => 1,
                'titulo_video' => 'Sobre o React',
                'duracao_video' => '00:05:15',
                'link_video' => 'M1A02.mp4',
                'thumb_video' => 'modulo2.png',
            ],
            [
                'modulo_id' => 1,
                'titulo_video' => 'Começando a trabalhar com React',
                'duracao_video' => '00:11:24',
                'link_video' => 'M1A03.mp4',
                'thumb_video' => 'modulo2.png',
            ],
            [
                'modulo_id' => 1,
                'titulo_video' => 'Criando elementos aninhados',
                'duracao_video' => '00:04:34',
                'link_video' => 'M1A04.mp4',
                'thumb_video' => 'modulo2.png',
            ],
            [
                'modulo_id' => 1,
                'titulo_video' => 'Conhecendo o JSX',
                'duracao_video' => '00:09:52',
                'link_video' => 'M1A05.mp4',
                'thumb_video' => 'modulo2.png',
            ],
            [
                'modulo_id' => 1,
                'titulo_video' => 'Aninhando com JSX e criando componentes',
                'duracao_video' => '00:10:54',
                'link_video' => 'M1A06.mp4',
                'thumb_video' => 'modulo2.png',
            ],
    		[
    			'modulo_id' => 2,
    			'titulo_video' => 'Apresentado minhas configurações de ambiente e o Webpack',
    			'duracao_video' => '00:03:20',
    			'link_video' => 'M2A01.mp4',
    			'thumb_video' => 'modulo2.png',
    		],
    		[
    			'modulo_id,' => 2,
    			'titulo_video,' => 'Aula do futuro',
    			'duracao_video,' => '00:05:11',
    			'link_video,' => 'M2A02.mp4',
    			'thumb_video,' => 'modulo2.png',
    		],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Configuração básica do Webpack',
                'duracao_video,' => '00:07:58',
                'link_video,' => 'M2A03.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Usando o server do Webpack',
                'duracao_video,' => '00:05:00',
                'link_video,' => 'M2A04.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Modularizando a aplicação',
                'duracao_video,' => '00:03:51',
                'link_video,' => 'M2A05.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Criando uma aplicação em React',
                'duracao_video,' => '00:08:28',
                'link_video,' => 'M2A06.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Usando o sistema de módulos do ES6E5',
                'duracao_video,' => '00:14:39',
                'link_video,' => 'M2A07.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Aula do futuro sobre ES Modules',
                'duracao_video,' => '00:09:53',
                'link_video,' => 'M2A08.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Configurando JSX no babel e sourcemaps no Webpack',
                'duracao_video,' => '00:06:52',
                'link_video,' => 'M2A09.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Configurando nossa aplicação para usar o React hot loader',
                'duracao_video,' => '00:11:12',
                'link_video,' => 'M2A10.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Colocando o hot loader para funcionar',
                'duracao_video,' => '00:12:43',
                'link_video,' => 'M2A11.mp4',
                'thumb_video,' => 'modulo2.png',
            ],
            [
                'modulo_id,' => 2,
                'titulo_video,' => 'Configurando a ferramenta de lint',
                'duracao_video,' => '00:08:16',
                'link_video,' => 'M2A12.mp4',
                'thumb_video,' => 'modulo2.png',
            ]
    	]);
    }
}
